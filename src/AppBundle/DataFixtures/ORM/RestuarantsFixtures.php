<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Restaurant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class RestuarantsFixtures extends Fixture
{

    public const RESTOUARANT_1 = 'Arabica_city';
    public const RESTOUARANT_2 = 'Красти_Краб';
    public const RESTOUARANT_3 = 'Чам_Баккет';
  /**
   * Load data fixtures with the passed EntityManager
   *
   * @param ObjectManager $manager
   * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
   */

    public function load(ObjectManager $manager)
    {

        $restaurant = new Restaurant();
        $restaurant
            ->setName('Arabica City')
            ->setImage('ресторанчик1.jpeg')
            ->setDescription('Супер дорогой ресторан, не ходите сюда');

        $manager->persist($restaurant);
        $this->addReference(self::RESTOUARANT_1, $restaurant);

        $restaurant = new Restaurant();
        $restaurant
            ->setName('Красти Крабс')
            ->setImage('Красти_краб.jpeg')
            ->setDescription('Самый крутой ресторан на дне морском');

        $manager->persist($restaurant);
        $this->addReference(self::RESTOUARANT_2, $restaurant);
        $restaurant = new Restaurant();
        $restaurant
            ->setName('Чам Баккет')
            ->setImage('Чам_баккет.png')
            ->setDescription('Самый плохой ресторан на дне морском');

        $manager->persist($restaurant);
        $this->addReference(self::RESTOUARANT_3, $restaurant);
        $manager->flush();

    }

}
