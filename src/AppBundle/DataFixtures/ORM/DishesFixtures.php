<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Dish;
use AppBundle\Entity\Restaurant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class DishesFixtures extends Fixture implements DependentFixtureInterface
{

    public const DISHES = 'dish';
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */


    public function load(ObjectManager $manager)
    {
        /** @var Restaurant $restaurant_1 */
        $restaurant_1 = $this->getReference(RestuarantsFixtures::RESTOUARANT_1);
        /** @var Restaurant $restaurant_2 */
        $restaurant_2 = $this->getReference(RestuarantsFixtures::RESTOUARANT_2);
        /** @var Restaurant $restaurant_3 */
        $restaurant_3 = $this->getReference(RestuarantsFixtures::RESTOUARANT_3);

        $count = 0;
        $dishes = ['Борстч','Салат','Рыба','Плов','Суп'];
       foreach ($dishes as $dish){
           $count++;
            $restaurantDish = new Dish();
            $restaurantDish
                ->setPrice(rand(100,1000))
                ->setName($dish)
                ->setImage($dish.'.jpeg')
                ->setRestaurant($restaurant_1);
            $manager->persist($restaurantDish);
           $this->addReference(self::DISHES.$count, $restaurantDish);
        }

        $dishes = ['Борстч','Салат','Рыба','Плов','Суп'];
        foreach ($dishes as $dish){
            $count++;
            $restaurantDish = new Dish();
            $restaurantDish
                ->setPrice(rand(100,1000))
                ->setName($dish)
                ->setImage($dish.'.jpeg')
                ->setRestaurant($restaurant_2);
            $manager->persist($restaurantDish);
            $this->addReference(self::DISHES.$count, $restaurantDish);
        }

        $dishes = ['Борстч','Салат','Рыба','Плов','Суп'];
        foreach ($dishes as $dish){
            $count++;
            $restaurantDish = new Dish();
            $restaurantDish
                ->setPrice(rand(100,1000))
                ->setName($dish)
                ->setImage($dish.'.jpeg')
                ->setRestaurant($restaurant_3);
            $manager->persist($restaurantDish);
            $this->addReference(self::DISHES.$count, $restaurantDish);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            RestuarantsFixtures::class
        ];
    }
}