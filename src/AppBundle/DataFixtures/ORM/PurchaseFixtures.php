<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Purchase;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Validator\Constraints\DateTime;

class PurchaseFixtures extends Fixture implements DependentFixtureInterface
{


    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Doctrine\Common\DataFixtures\BadMethodCallException
     */

    public function load(ObjectManager $manager)
    {

        $d = new \DateTime;

        for($i = 1; $i < 16; $i++){
            $purchase = new Purchase();
            $purchase
                ->setDate($d)
                ->setDishes($this->getReference(DishesFixtures::DISHES.$i));
            $manager->persist($purchase);

        }

        for($i = 1; $i < 20; $i++){
            $purchase = new Purchase();
            $purchase
                ->setDate($d)
                ->setDishes($this->getReference(DishesFixtures::DISHES.'11'));
            $manager->persist($purchase);

            $purchase = new Purchase();
            $purchase
                ->setDate($d)
                ->setDishes($this->getReference(DishesFixtures::DISHES.'3'));
            $manager->persist($purchase);

            $purchase = new Purchase();
            $purchase
                ->setDate($d)
                ->setDishes($this->getReference(DishesFixtures::DISHES.'2'));
            $manager->persist($purchase);

            $purchase = new Purchase();
            $purchase
                ->setDate($d)
                ->setDishes($this->getReference(DishesFixtures::DISHES.'13'));
            $manager->persist($purchase);

            $purchase = new Purchase();
            $purchase
                ->setDate($d)
                ->setDishes($this->getReference(DishesFixtures::DISHES.'7'));
            $manager->persist($purchase);

            $purchase = new Purchase();
            $purchase
                ->setDate($d)
                ->setDishes($this->getReference(DishesFixtures::DISHES.'5'));
            $manager->persist($purchase);

        }


        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            DishesFixtures::class
        ];
    }
}
