<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Dish;
use AppBundle\Entity\Restaurant;
use AppBundle\Form\DishForm;
use AppBundle\Form\RestourantForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/")
 */
class DishesController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {

        $popularDishes = [];

        $PlacesFoReating = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Restaurant')
            ->findAll();

        foreach ($PlacesFoReating as $place){

            $dishes = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Dish')
                ->getMostPopularDishes($place,2);


            $popularDishes[] = [
                'restaurant' => $place,
                'dishes' => $dishes
            ];

        }

        return $this->render('@App/Dishes/index.html.twig', array(
            'popularDishesPlaces' => $popularDishes
        ));
    }

    /**
     * @Route("/newRestourant")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newRestaurantAction(Request $request){

        $restaurant = new Restaurant();
        $form = $this->createForm(
            RestourantForm::class,
            $restaurant);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($restaurant);
            $em->flush();

            return $this->redirectToRoute('app_dishes_index');
        }

        return $this->render('@App/Dishes/newRestaurant.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/dishes/new/{id}", requirements={"id": "\d+"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newDishAction(Request $request, int $id){

        $dish = new Dish();

        /** @var Restaurant $restaurant */
        $restaurant = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurant')
            ->find($id);

        $form = $this->createForm(
            DishForm::class,
            $dish);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $dish->setRestaurant($restaurant);

            $em = $this->getDoctrine()->getManager();

            $em->persist($dish);
            $em->flush();
            return $this->redirectToRoute('app_dishes_index', array('id' => $restaurant->getId()));
        }

        return $this->render('@App/Dishes/newDish.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/dishes/{id}", requirements={"id": "\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dishesLisAction(int $id){

        $restaurant = $this->getDoctrine()
            ->getRepository('AppBundle:Restaurant')
            ->find($id);

        $dishes = $restaurant->getDishes();
        return $this->render('@App/Dishes/dishesList.html.twig', array(
            'dishes' => $dishes
        ));
    }

}
